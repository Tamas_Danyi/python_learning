# hibakezeles: try, except, else, finally
try:
    print(1)
    print("hello")
    print(x)
    print("vege")
except NameError:
    print("elfelejtettel egy valtozot definialni")
except:
    print("valami hiba tortent")
else:
    print("nem volt hiba")
finally:
    print("ez meg mindig lefut")


def finally_miert():
    try:
        # megnyitok egy fajlt irasra
        # beleirom a felhasznalotol kapott szamokat
        pass

    finally:
        # bezarom a fajlt
        pass


def osztas(a, b):
    return a / b


try:
    print(osztas(25, 0))
except ZeroDivisionError:
    pass

# hibak dobasa/emelese -- throw/raise an exception
szam = 0
try:
    if szam == 0:
        raise ZeroDivisionError
    else:
        print(f"a szam: {szam}")
except ZeroDivisionError:
    print("Ez a szam vacak")
