# Simple pygame program

# Import and initialize the pygame library
import pygame

pygame.init()  # felkesziti a pygame modult, pl elkezdi figyelni az esemenyeket a hatterben

# Set up the drawing window
screen = pygame.display.set_mode([500, 500])

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        print(event)  # kiirjuk, hogy milyen esemenyek
        # pygame.QUIT : windows X bezaras
        # pygame.KEYDOWN : billentyu lenyomas
        # event.key : melyik billentyu?
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False

    # Fill the background with white
    screen.fill((255, 255, 255))  # RGB ertek, feher

    # Draw a solid blue circle in the center
    pygame.draw.circle(screen, (255, 0, 0), (250, 250), 150)
    pygame.draw.circle(screen, (255, 255, 255), (250, 250), 100)
    pygame.draw.circle(screen, (0, 255, 0), (250, 250), 50)
    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

# Done! Time to quit.
pygame.quit()
