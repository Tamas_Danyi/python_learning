from math import sin, cos, radians

import pygame

pygame.init()
wn = pygame.display.set_mode((600, 600))

r = 100
a = 300
b = 200

clock = pygame.time.Clock()

running = True
colors = ["red", "white", "green"]
counter = 0


def has_ended():
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            return True
    return False


while running:
    for i in range(0, 360):
        if has_ended():
            running = False
            break
        clock.tick(200)
        index = counter % len(colors)
        pygame.draw.circle(wn, colors[index],
                           (int(r * cos(radians(i)) + a), int(r * sin(radians(i)) + b)), 4)
        pygame.display.update()
    counter += 1

pygame.quit()
