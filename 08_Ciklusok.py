from dt_util import ellenorizd_le

print('===================== FOR CIKLUS ===========================')
print("= amikor egy kollekcion vegig szeretnenk lepkedni/iteralni =")
print("= amikor elore tudjuk, hogy hanyszor szeretnenk, ha lefusson egy programkod =")


def print_all(printable):
    print("vegigiteralunk a kovetkezon:", type(printable), printable)
    for item in printable:
        print("elem:", item)
    print("=== item erteke a ciklus futasa utan:", item)


def print_dict(dictionary):
    print("print_dict:")
    for key in dictionary.keys():
        print("kulcs:", key)
        print("kulcs:", key, "ertek:", dictionary[key])
    print("---")
    for value in dictionary.values():
        print("ertek:", value)
    print("---")
    for (k, v) in dictionary.items():  # [(k1,v1), (k2,v2),...]
        print("kulcs:", k, "ertek:", v)
    print("---")


print_all(["alma", "barack", "hangya", "hajajaj", "alma"])
print_all({"alma", "barack", "hangya", "hajajaj", "alma"})
print_all(("alma", "barack", "hangya", "hajajaj", "alma"))
print_all({1: "alma", 2: "barack", "3": "hangya", "negy": "hajajaj", "alma": "alma"})
print_dict({1: "alma", 2: "barack", "3": "hangya", "negy": "hajajaj", "alma": "alma"})  # mit ir ki?

print("====")
for i in range(5):
    print("x")

print(range(5))
print(list(range(5)))
print(type(range(5)))

print("====")
for i in range(10):
    print("i:", i)

print("====")
for j in range(1, 10):
    print("j:", j)

print("====")
for k in range(1, 10, 3):
    print("k:", k)

print("====")
gyumi = "alma"
for i in gyumi:
    print(i)

for i in range(len(gyumi)):
    print(gyumi[i])

#
print("====")
for j in range(5):
    print(j)
    if j == 3:
        break
    print("paros", j % 2 == 0, sep=":")

print("====")
for j in range(5):
    print(j, end=" ")
    if j == 3:
        continue
    print("paros", j % 2 == 0, sep=":")

print('===================== WHILE CIKLUS ====================')
print('= amikor előre fel tudunk írni egy leállási feltételt =')

i = 1
while i < 6:
    print(i)
    i += 1
#     break es continue itt is hasznalhato

# Irj egy funkciot, ami 1-tol 10-ig osszeadja a szamokat, es az eredmennyel visszater
# 1. while
# 2. for

def feladat_for(szamtol, szamig):
    sum = 0
    for i in range(szamtol, szamig + 1):
        sum += i
    return sum


def feladat_while(szamtol, szamig):
    sum = 0
    while szamtol <= szamig:
        sum += szamtol
        szamtol += 1
    return sum


def feladat_while_true(szamtol, szamig):
    sum = 0
    while True:
        sum += szamtol
        szamtol += 1
        if szamtol > szamig:  # kilepesi feltetel
            break  # a break itt az ot korbefogo ciklust, a while-t szakitja meg
    return sum


print(feladat_for(1, 10))
print(feladat_while(1, 10))
print(feladat_while_true(1, 10))


def duplazo_sor_for(start, db):
    result = []
    for i in range(db):
        result.append(start)
        start *= 2
    return result


def duplazo_sor_while(start, limit):
    result = []
    while start < limit:
        result.append(start)
        start *= 2
    return result


def duplazo_sor_while_with_counter(start, limit):
    result = []
    counter = 0
    while start < limit:
        result.append(start)
        start *= 2
        counter += 1
    return (result, counter)


print(duplazo_sor_for(1, 11))
print(duplazo_sor_while(1, 5000))
print(duplazo_sor_while_with_counter(1, 5000))

print("==============")
print(" Hazi feladat: a parameterul kapott listabol valogasd ki a paros/paratlan elemeket for/while ciklusok felhasznalasaval")
print("==============")


def kivalogat_paros_for(lista):
    eredmeny = []
    # IDE IRD A KODOT! nem egy sor :)
    return eredmeny


def kivalogat_paratlan_while(lista):
    eredmeny = []
    # IDE IRD A KODOT! nem egy sor :)
    return eredmeny


# Ezeket se bantsd, ez fogja leellenorizni, hogy helyesen oldottad-e meg a feladatot
lista = list(range(3, 18, 3))
ellenorizd_le([6, 12, 18], kivalogat_paros_for(lista))
ellenorizd_le([3, 9, 15], kivalogat_paratlan_while(lista))
