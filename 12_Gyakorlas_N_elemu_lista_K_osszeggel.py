import random

# Irj egy programot, amelyben a felhasználótól bekérsz 2 számot:
# 1.: mennyi legyen az eredmény lista elemeinek az összege
# 2.: hány eleme legyen a listának
# eredmeny: maga a lista

sum = int(input("szamok osszege: "))
count = int(input("elemek szama: "))


def eddigi_osszeg(lista):
    s = 0
    for l in lista:
        s += l
    return s


def feladat(sum, count):
    list = []

    for num in range(count - 1):
        while True:
            x = random.randint(0, sum)
            if eddigi_osszeg(list) + x < sum:
                list.append(x)
                break

    list.append(sum - eddigi_osszeg(list))

    return list


result = feladat(sum, count)
result.sort()
print(result, eddigi_osszeg(result))

# x = feladat(22,5) #[1,7,2,4,8]
