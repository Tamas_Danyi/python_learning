import pygame

pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

kep_szelesseg = 500
kep_magassag = 500
screen = pygame.display.set_mode([kep_szelesseg, kep_magassag])


def cica(bal_kezd=0, felso_kezd=0, meret_szorzo=0.4):
    def elipszis(x, y, szelesseg, magassag, szin):
        pygame.draw.ellipse(screen, szin,
                            (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                             szelesseg * meret_szorzo, magassag * meret_szorzo))

    def koriv(x, y, szelesseg, magassag, a1, a2, szin):
        pygame.draw.arc(screen, szin,
                        (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                         szelesseg * meret_szorzo, magassag * meret_szorzo)
                        , a1, a2)

    def vonal(x1, y1, x2, y2, vastagsag, szin):
        pygame.draw.line(screen, szin,
                         (dx + bal_kezd + x1 * meret_szorzo, dy + felso_kezd + y1 * meret_szorzo),
                         (dx + bal_kezd + x2 * meret_szorzo, dy + felso_kezd + y2 * meret_szorzo),
                         vastagsag)

    def fej():
        elipszis(10, 20, 110, 85, black)
        elipszis(20, 0, 33, 85, black)
        elipszis(76, 0, 33, 85, black)

    def szem():
        koriv(30, 40, 30, 10, 0, 3.2, white)
        koriv(30, 40, 30, 10, 0, 6.4, white)
        koriv(75, 40, 30, 10, 0, 3.2, white)
        koriv(75, 40, 30, 10, 0, 6.4, white)
        elipszis(41, 40, 7, 10, white)
        elipszis(86, 40, 7, 10, white)

    def orr_es_szaj():
        elipszis(64, 65, 10, 8, white)
        vonal(67, 65, 67, 81, 2, white)
        koriv(59, 81, 20, 20, 0, 3.14, white)

    def bajusz():
        vonal(10, 66, 120, 66, 2, white)
        vonal(0, 66, 10, 66, 2, black)
        vonal(120, 66, 130, 66, 2, black)
        vonal(10, 59, 120, 71, 2, white)
        vonal(0, 59, 10, 60, 2, black)
        vonal(120, 70, 130, 71, 2, black)
        vonal(10, 71, 120, 59, 2, white)
        vonal(0, 71, 10, 70, 2, black)
        vonal(120, 60, 130, 59, 2, black)

    fej()
    szem()
    bajusz()
    orr_es_szaj()


running = True
is_mouse_down = False
dx = 0
dy = 0

while running:

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                if dy - 6 >= 0:
                    dy -= 6
            elif event.key == pygame.K_DOWN:
                if dy + 6 <= 500:
                    dy += 6
            elif event.key == pygame.K_LEFT:
                if dx - 6 >= 0:
                    dx -= 6
            elif event.key == pygame.K_RIGHT:
                if dx + 6 <= 500:
                    dx += 6

            print("eltolas:", dx, dy)

        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False

    screen.fill(white)  # RGB ertek, feher

    pygame.draw.line(screen, black, (50, 50), (50, 200), 1) # függőleges
    pygame.draw.line(screen, black, (100, 0), (100, 150), 1) # függőleges
    pygame.draw.line(screen, black, (50, 200), (150, 200), 1) # vízszintes
    pygame.draw.line(screen, black, (100, 100), (350, 100), 1) # vízszintes
    pygame.draw.line(screen, black, (150, 50), (450, 50), 1) # vízszintes
    pygame.draw.line(screen, black, (150, 150), (300, 150), 1) # vízszintes
    pygame.draw.line(screen, black, (450, 50), (450, 100), 1) # függőleges
    pygame.draw.line(screen, black, (400, 50), (400, 150), 1) # függőleges
    pygame.draw.line(screen, black, (400, 150), (500, 150), 1) # vízszintes
    pygame.draw.line(screen, black, (350, 100), (350, 200), 1) # függőleges
    pygame.draw.line(screen, black, (0, 250), (200, 250), 1) # vízszintes
    pygame.draw.line(screen, black, (200, 200), (250, 200), 1)  # vízszintes
    pygame.draw.line(screen, black, (350, 100), (350, 200), 1)  # függőleges
    pygame.draw.line(screen, black, (200, 200), (200, 250), 1)  # függőleges
    pygame.draw.line(screen, black, (250, 200), (250, 250), 1)  # függőleges
    pygame.draw.line(screen, black, (150, 150), (150, 200), 1)  # függőleges
    pygame.draw.line(screen, black, (300, 150), (300, 250), 1)  # függőleges
    pygame.draw.line(screen, black, (350, 200), (450, 200), 1)  # vízszintes
    pygame.draw.line(screen, black, (300, 250), (400, 250), 1)  # vízszintes
    pygame.draw.line(screen, black, (50, 300), (50, 400), 1)  # függőleges
    pygame.draw.line(screen, black, (100, 250), (100, 450), 1)  # függőleges
    pygame.draw.line(screen, black, (50, 450), (100, 450), 1)  # vízszintes
    pygame.draw.line(screen, black, (400, 250), (400, 300), 1)  # függőleges
    pygame.draw.line(screen, black, (450, 250), (450, 400), 1)  # függőleges
    pygame.draw.line(screen, black, (0, 400), (50, 400), 1)  # vízszintes
    pygame.draw.line(screen, black, (150, 300), (400, 300), 1)  # vízszintes
    pygame.draw.line(screen, black, (150, 300), (150, 500), 1)  # függőleges
    pygame.draw.line(screen, black, (200, 350), (200, 450), 1)  # függőleges
    pygame.draw.line(screen, black, (200, 350), (450, 350), 1)  # vízszintes
    pygame.draw.line(screen, black, (250, 400), (350, 400), 1)  # vízszintes
    pygame.draw.line(screen, black, (200, 450), (300, 450), 1)  # vízszintes
    pygame.draw.line(screen, black, (400, 450), (500, 450), 1)  # vízszintes
    pygame.draw.line(screen, black, (350, 400), (350, 500), 1)  # függőleges
    pygame.draw.line(screen, black, (400, 350), (400, 450), 1)  # függőleges
    pygame.draw.line(screen, red, (450, 450), (500, 500), 5)
    pygame.draw.line(screen, red, (500, 450), (450, 500), 5)

    cica(0, 0)

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

# Done! Time to quit.
pygame.quit()