import pygame

pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

kep_szelesseg = 1000
kep_magassag = 800
screen = pygame.display.set_mode([kep_szelesseg, kep_magassag])


def cica(bal_kezd=0, felso_kezd=0, meret_szorzo=1):
    def elipszis(x, y, szelesseg, magassag, szin):
        pygame.draw.ellipse(screen, szin,
                            (bal_kezd + x * meret_szorzo, felso_kezd + y * meret_szorzo,
                             szelesseg * meret_szorzo, magassag * meret_szorzo))

    def koriv(x, y, szelesseg, magassag, a1, a2, szin):
        pygame.draw.arc(screen, szin,
                        (bal_kezd + x * meret_szorzo, felso_kezd + y * meret_szorzo,
                         szelesseg * meret_szorzo, magassag * meret_szorzo)
                        , a1, a2)

    def vonal(x1, y1, x2, y2, vastagsag, szin):
        pygame.draw.line(screen, szin,
                         (bal_kezd + x1 * meret_szorzo, felso_kezd + y1 * meret_szorzo),
                         (bal_kezd + x2 * meret_szorzo, felso_kezd + y2 * meret_szorzo),
                         vastagsag)

    def fej():
        elipszis(10, 20, 110, 85, black)
        elipszis(20, 0, 33, 85, black)
        elipszis(76, 0, 33, 85, black)

    def szem():
        koriv(30, 40, 30, 10, 0, 3.2, white)
        koriv(30, 40, 30, 10, 0, 6.4, white)
        koriv(75, 40, 30, 10, 0, 3.2, white)
        koriv(75, 40, 30, 10, 0, 6.4, white)
        elipszis(41, 40, 7, 10, white)
        elipszis(86, 40, 7, 10, white)

    def orr_es_szaj():
        elipszis(64, 65, 10, 8, white)
        vonal(67, 65, 67, 81, 2, white)
        koriv(59, 81, 20, 20, 0, 3.14, white)

    def bajusz():
        vonal(10, 66, 120, 66, 2, white)
        vonal(0, 66, 10, 66, 2, black)
        vonal(120, 66, 130, 66, 2, black)
        vonal(10, 59, 120, 71, 2, white)
        vonal(0, 59, 10, 60, 2, black)
        vonal(120, 70, 130, 71, 2, black)
        vonal(10, 71, 120, 59, 2, white)
        vonal(0, 71, 10, 70, 2, black)
        vonal(120, 60, 130, 59, 2, black)

    fej()
    szem()
    bajusz()
    orr_es_szaj()


running = True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False

    screen.fill(white)  # RGB ertek, feher

    cica(0, 0)
    cica(140, 0, 1)
    cica(280, 0, 2)
    cica(560, 0, 3)

    # cica_szelesseg = 130
    # cica_magassag = 110
    # bal_behuzas = 0
    # szorzo = 0.01
    # for sz in range(10):
    #     fenti_behuzas = 0
    #     while fenti_behuzas < kep_magassag:
    #         cica(bal_behuzas, fenti_behuzas, szorzo)
    #         fenti_behuzas += szorzo * cica_magassag
    #
    #     bal_behuzas += szorzo * cica_szelesseg
    #     szorzo *= 2

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

# Done! Time to quit.
pygame.quit()
