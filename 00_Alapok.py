# Ez egy egysoros komment, ami csak az olvasónak szól. #-tol, sor végéig tart.


"""
Ez egy többsoros komment. 3 macskaköröm az elején, 3 a végén.
"""


'''
Ilyen aposztróf is jó a macskaköröm helyett. A Python nem tesz különbséget.
'''


print("PyCharm-ba betöltve ezt a fájlt, Shift+F10-zel lefuttatjuk. Vagy a zöld play gombbal.")


'''
Ha a program futtatása végén ez van, akkor nem volt hiba. 
 
 Process finished with exit code 0
 
Ha a kód nem 0, akkor valami hiba történt.
'''


