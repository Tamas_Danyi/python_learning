import random

import pygame

clock = pygame.time.Clock()
pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

kep_szelesseg = 500
kep_magassag = 500
screen = pygame.display.set_mode([kep_szelesseg, kep_magassag])
step = 1
running = True
is_mouse_down = False
dx = 0
dy = 0
pressed_keys = {}
# mozgasi lehetosegeket tarolo valtozok
mehet_fel = True
mehet_le = True
mehet_balra = True
mehet_jobbra = True
# targyak amikkel utkozhet a cica
cseresznye = None


def uj_eger():
    filename = 'mice.png'
    eger = pygame.image.load(filename)
    eger = pygame.transform.scale(eger, (50, 50))
    return eger


def print_rect(rect, name="rect"):
    print(name, "balfelso:" + str(rect.topleft) + " - jobbalso:" + str(rect.bottomright), rect)


def cica(bal_kezd=0, felso_kezd=0, meret_szorzo=0.37, labirintus=None):
    global mehet_fel, mehet_le, mehet_jobbra, mehet_balra

    def elipszis(x, y, szelesseg, magassag, szin):
        return pygame.draw.ellipse(screen, szin,
                                   (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                                    szelesseg * meret_szorzo, magassag * meret_szorzo))

    def koriv(x, y, szelesseg, magassag, a1, a2, szin):
        return pygame.draw.arc(screen, szin,
                               (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                                szelesseg * meret_szorzo, magassag * meret_szorzo)
                               , a1, a2)

    def vonal(x1, y1, x2, y2, vastagsag, szin):
        return pygame.draw.line(screen, szin,
                                (dx + bal_kezd + x1 * meret_szorzo, dy + felso_kezd + y1 * meret_szorzo),
                                (dx + bal_kezd + x2 * meret_szorzo, dy + felso_kezd + y2 * meret_szorzo),
                                vastagsag)

    def fej():
        fej = elipszis(10, 20, 110, 85, black)
        fej.union(elipszis(20, 0, 33, 85, black))
        fej.union(elipszis(76, 0, 33, 85, black))
        return fej

    def szem():
        szem = koriv(30, 40, 30, 10, 0, 3.2, white)
        szem.union(koriv(30, 40, 30, 10, 0, 6.4, white))
        szem.union(koriv(75, 40, 30, 10, 0, 3.2, white))
        szem.union(koriv(75, 40, 30, 10, 0, 6.4, white))
        szem.union(elipszis(41, 40, 7, 10, white))
        szem.union(elipszis(86, 40, 7, 10, white))
        return szem

    def orr_es_szaj():
        orr = elipszis(64, 65, 10, 8, white)
        orr.union(vonal(67, 65, 67, 81, 2, white))
        orr.union(koriv(59, 81, 20, 20, 0, 3.14, white))
        return orr

    def bajusz():
        bajusz = vonal(10, 66, 120, 66, 2, white)
        bajusz.union(vonal(0, 66, 10, 66, 2, black))
        bajusz.union(vonal(120, 66, 130, 66, 2, black))
        bajusz.union(vonal(10, 59, 120, 71, 2, white))
        bajusz.union(vonal(0, 59, 10, 60, 2, black))
        bajusz.union(vonal(120, 70, 130, 71, 2, black))
        bajusz.union(vonal(10, 71, 120, 59, 2, white))
        bajusz.union(vonal(0, 71, 10, 70, 2, black))
        bajusz.union(vonal(120, 60, 130, 59, 2, black))
        return bajusz

    cica = fej()
    cica.union(szem())
    cica.union(bajusz())
    cica.union(orr_es_szaj())

    # ha utkozik a macska a fallal, akkor letiltjuk a megfelelo mozgasi lehetoseget
    for v in labirintus:
        utkozhet_fuggolegesen = v.top < cica.bottom and cica.top < v.bottom  # egyebkent elmenne mellette
        if utkozhet_fuggolegesen:
            if abs(cica.left - v.right) < step:
                mehet_balra = False
            if abs(v.left - cica.right) < step:
                mehet_jobbra = False

        utkozhet_vizszintesen = v.left < cica.right and cica.left < v.right  # egyebkent elmenne mellette
        if utkozhet_vizszintesen:
            if abs(v.bottom - cica.top) < step:
                mehet_fel = False
            if abs(cica.bottom - v.top) < step:
                mehet_le = False

    return cica


def event_handling():
    global running
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            pressed_keys[event.key] = True
        elif event.type == pygame.KEYUP:
            pressed_keys[event.key] = False
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False


def key_handling():
    global dx, dy
    for key in pressed_keys:
        if pressed_keys[key]:
            if key == pygame.K_UP:
                if dy - step >= 0 and mehet_fel:
                    dy -= step
            elif key == pygame.K_DOWN:
                if dy + step <= 460 and mehet_le:
                    dy += step
            elif key == pygame.K_LEFT:
                if dx - step >= 0 and mehet_balra:
                    dx -= step
            elif key == pygame.K_RIGHT:
                if dx + step <= 450 and mehet_jobbra:
                    dx += step


def labirintus():
    def labirintus_vonal(p1, p2):
        return pygame.Rect(p1[0], p1[1], max(abs(p2[0] - p1[0]), 1), max(abs(p2[1] - p1[1]), 1))

    return [
        labirintus_vonal((50, 50), (50, 200)),  # függőleges
        labirintus_vonal((100, 0), (100, 150)),  # függőleges
        labirintus_vonal((50, 200), (150, 200)),  # vízszintes
        labirintus_vonal((100, 100), (350, 100)),  # vízszintes
        labirintus_vonal((150, 50), (450, 50)),  # vízszintes
        labirintus_vonal((150, 150), (300, 150)),  # vízszintes
        labirintus_vonal((450, 50), (450, 100)),  # függőleges
        labirintus_vonal((400, 50), (400, 150)),  # függőleges
        labirintus_vonal((400, 150), (500, 150)),  # vízszintes
        labirintus_vonal((350, 100), (350, 200)),  # függőleges
        labirintus_vonal((0, 250), (200, 250)),  # vízszintes
        labirintus_vonal((200, 200), (250, 200)),  # vízszintes
        labirintus_vonal((350, 100), (350, 200)),  # függőleges
        labirintus_vonal((200, 200), (200, 250)),  # függőleges
        labirintus_vonal((250, 200), (250, 250)),  # függőleges
        labirintus_vonal((150, 150), (150, 200)),  # függőleges
        labirintus_vonal((300, 150), (300, 250)),  # függőleges
        labirintus_vonal((350, 200), (450, 200)),  # vízszintes
        labirintus_vonal((300, 250), (400, 250)),  # vízszintes
        labirintus_vonal((50, 300), (50, 400)),  # függőleges
        labirintus_vonal((100, 250), (100, 450)),  # függőleges
        labirintus_vonal((50, 450), (100, 450)),  # vízszintes
        labirintus_vonal((400, 250), (400, 300)),  # függőleges
        labirintus_vonal((450, 250), (450, 400)),  # függőleges
        labirintus_vonal((0, 400), (50, 400)),  # vízszintes
        labirintus_vonal((150, 300), (400, 300)),  # vízszintes
        labirintus_vonal((150, 300), (150, 500)),  # függőleges
        labirintus_vonal((200, 350), (200, 450)),  # függőleges
        labirintus_vonal((200, 350), (450, 350)),  # vízszintes
        labirintus_vonal((250, 400), (350, 400)),  # vízszintes
        labirintus_vonal((200, 450), (300, 450)),  # vízszintes
        labirintus_vonal((400, 450), (500, 450)),  # vízszintes
        labirintus_vonal((350, 400), (350, 500)),  # függőleges
        labirintus_vonal((400, 350), (400, 450))  # függőleges
    ]


def uj_cseresznye(x, y):
    print("uj cseresznye: ", x, y)
    return pygame.Rect(x, y, 5, 5)


def rajzolj_labirintust(labirintus):
    for v in labirintus:
        pygame.draw.rect(screen, black, v)


labi = labirintus()

while running:
    # minden ciklus elejen bealitjuk az alapertelmezett mozgasi ertekeket, amiket majd a 'labirintus_vonal' funkcioban modositunk
    mehet_jobbra = True
    mehet_balra = True
    mehet_fel = True
    mehet_le = True

    event_handling()

    screen.fill(white)  # RGB ertek, feher

    eger = uj_eger()
    eger_rect = eger.get_rect()
    eger_rect.center = (475, 475)
    screen.blit(eger, eger_rect)

    cicc = cica(0, 0, labirintus=labi)

    rajzolj_labirintust(labi)

    if cseresznye == None:
        cseresznye = uj_cseresznye(random.randint(0, 500), random.randint(0, 500))

    if cicc.colliderect(cseresznye):
        print("cseresznye torolve: ", cseresznye.x, cseresznye.y)
        cseresznye = None
    else:
        pygame.draw.rect(screen, red, cseresznye)

    key_handling()

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

    clock.tick(100)

# Done! Time to quit.
pygame.quit()
