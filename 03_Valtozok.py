# szintaxis:
# valtozo_nev = kifejezés
#
# A jobb oldalon levő kifejezés kiértékelődik, majd a végső értéket megkapja a 'valtozo_nev' nevű változó értékként
# Konvenció szerint a változók kisbetűvel kezdődnek, és ha több szóból állnak, akkor aláhúzással választjuk el a szavakat

a = "2"
ertek1 = 'szoveg'  # pl egy szöveg - string típusú érték
print(ertek1, type(ertek1))
ertek1 = 1  # de most már dinamikusan megváltoztattam számmá
print(ertek1, type(ertek1))
ertek2 = ertek1  # de lehet egy másik változó is az = jobb oldalán álló kifejezés; az ertek1 válzozó kiértékelődik, majd az értéke belekerül az ertek3-ba
print(ertek2, type(ertek2))
ertek3 = 1 + 3  # lehet a kifejezés egy összetett kifejezés is; kiértékelődés után ennek az eredménye is megy a változóba
print(ertek3, type(ertek3))
ertek4 = 2.5 + ertek3  # a változók és az értékek szabadon kombinálhatók, amíg a típusaik megfelelőek
print(ertek4, type(ertek4))
# EZ UTÓBBI VISZONT SOKSZOR CSAK FUTÁSI IDŐBEN DERÜL KI
# fordítási idő: amikor írod a kódot
# futtatási idő: amikor lefuttatod a kódot

print("\n\n")
# print(a + 3) # Mennyi lesz az eredmény? Mi az 'a' típusa? Konvertáld át int típusúvá int(a)-val


# Feladat
########################
# Próbáld meg lefuttatni a következő két sort! Így lehet a felhasználótól valami értéket bekérni, amit egy változóban tárolunk.
# valtozo = input("valami szoveg")
# print(valtozo)
########################
# Ez alapján írj egy programot, ami bekéri egy téglalap szélességét, aztán a magasságát, azután írd ki a téglalap kerületét és területét!

# Feladat
########################
szelesseg = int(input("szelesseg:"))
magassag = int(input("magassag:"))
print(type(szelesseg), type(magassag))
print("a teglalap terulete:", szelesseg * magassag)
print("a teglalap kerulete:", 2 * (szelesseg + magassag))
