# osztály (class) : összetett, komplex típusok
# objektum (object) : az osztály leírása alapján létrehozott konkrét dolog -- érték; az osztály példányosítása

# osztály: tervrajz, ami alapján az objektumokat konstruáljuk. pl egy tervrajz alapján több ház is készülhet, ami ugyanakkora, ugyanott vannak a nyílászárók, stb. 
# De a felépített házak a város különböző helyein állhatnak. Több példány létezik. Lehetnek különböző tulajdonságaik, pl különböző színűre festhetik a falakat.



a = [1, 66.6, 3, 6, "a"] # a: valtozo a globalis nevterben

def ird_ki(x): # ird_ki: funkcio a globalis nevterben
    print(a)
    for x in a:
        print(x, type(x))


# hozzunk letre egy osztalyt! szintaktika:
class Szemely:
    # attributumok, tulajdonsagok
    # milyen?
    nev = '-'
    eletkor = 0
    nem = '-'

    # hello metodus (method), az a funkcio, amit egy osztalyon belul hozunk letre, az egy metodus
    # mit csinal? mire kepes?
    def hello(self):
        return "hello " + self.nev


sz = Szemely()  # peldanyositjuk az osztalyt
sz.nev = "Megatron"
sz2 = sz
print(sz, type(sz))
print(sz2, type(sz2))
print("---")
print(sz.nev)
print(sz.hello())  # egyik objektumhoz tartozo hello
print("---")
sz3 = Szemely()
sz3.nev = "Ultron"
print(sz, type(sz))
print(sz2, type(sz2))
print(sz3, type(sz3))
print(sz3.hello())  # masik objektumhoz tartozo hello
print("---")


def hello(p):
    return "hello " + p


print(hello("Joska"))  # ugyanaz a hello fuggveny,
print(hello("Pista"))  # csak ket kulonbozo parameterrel


# Eddig is dolgoztunk osztalyokkal, szoval nem kell bexarni!
lista = [1, 3, 10, 4, 5]  # letrehozunk egy lista tipusu (class) objektumot
eredmeny = lista.reverse()  # a lista valtozoban eltarolt objektumnak a reverse nevu metodusat meghivom, aminek nincs return-je (return None)
print(lista, eredmeny)

# Hazi feladat:
# 2 listat csinalni:
# A/ osszegyujteni azokat a kifejezeseket, amik x.y(z) formatumban hasznaltunk eddig
# Figyelem! A 'z' parameter nem kotelezo, hanem opcionalis
# B/ Masik modulban definialt funkciok listaja, amiket eddig hasznaltunk


# Nehany egyszeru pelda az osztalyokra
class Negyzet:
    oldal = 0

    def kerulet(self):
        return 4 * self.oldal

    def terulet(self):
        return self.oldal * self.oldal

class Teglalap:
    alap = 0
    magassag = 0

    def kerulet(self):
        return 2 * (self.alap + self.magassag)

    def terulet(self):
        return self.alap * self.magassag
