'''
A 3 legfontosabb primitív adattípus:

magyarul        angolul     pythonban   példa
----------------------------------------------------------------------------------
szám            number      number      1, 1.5 (ponttal jelezzük a tizedesvesszőt)
szöveg          string      str         "ez egy szöveg", 'a', ""
logikai érték   boolean     bool        True, False
----------------------------------------------------------------------------------
ezek a programozás 1x1-es LEGO blokkjai
'''

print(4)  # egész szám --- integer / int
print(4.1)  # tört szám / lebegőpontos --- float
print("Ez egy szöveg")  # szöveg / string
print('''Ez egy 
többsoros
szöveg. Igen, olyan, mint a többsoros komment''')
print(True)  # logikai érték / boolean

print("")
print("====== témakör elválasztó ======")
print()

# type(valami) : megmondja, milyen típusú ez a "valami"
print(type(4))  # int (integer)
print(type(4.1))  # float
print(type("Ez egy szöveg"))  # str (string)
print(type('''Ez egy 
többsoros
szöveg. Igen, olyan, mint a többsoros komment'''))  # str
print(type(True))  # bool (boolean)

print("")
print("====== témakör elválasztó ======")
print()

# Feladatok
########################
# Futtasd le a következőket és magyarázd meg, mi a hiba. Ismerkedj a hibaüzenetekkel!
########################
# print("Szoveg eleje"a)
# print(a"Szoveg vege")
# print("Ebben a szövegben itt (") van egy macskaköröm, ami miatt hiba lesz.")
# print('Ebben a szövegben itt (') van egy aposztróf, ami miatt hiba lesz.')
# print("Ebben a szövegben viszont (') nincs hiba.")

# SZORGALMI :D
# print("Eleje (") vege") # Módosítsd ezt a sort, hogy ezt írja ki: "Eleje (") vége"
# a Google a barátod!

# Feladatok megoldása
########################
# print("Szoveg eleje"a) -- nem kerülhet karakter a macskaköröm és az idézőjel közé, mert azt a fordító nem tudja értelmezni
# print(a"Szoveg vege") -- ugyanez, csak az elején
# print("Ebben a szövegben itt (") van egy macskaköröm, ami miatt hiba lesz.") -- ha macskakörömben van a string, akkor nem lehet beletenni egy zárójelben lévő macskakörmöt, mert a program azt veszi a string végének és az utána következő részt nem tudja értelmezni a fordító
# print('Ebben a szövegben itt (') van egy aposztróf, ami miatt hiba lesz.') -- ugyanez a baj aposztróf használatakor is
# print("Ebben a szövegben viszont (') nincs hiba.") -- macskakörmös stringbe lehet tenni aposztrófot
# print("Eleje (\") vagy vege")

# \ speciális karakter, ami elveszi az utána következő karakter speciális jelentését
# Tehát \" kettő karakter, de egy " karaktert fog jelenteni, nem pedig a string végét
# Néhány fontosabb speciális karakter:
# \n : sortörés
# \t : tabulátor