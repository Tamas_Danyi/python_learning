import pygame

clock = pygame.time.Clock()
pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

kep_szelesseg = 500
kep_magassag = 500
screen = pygame.display.set_mode([kep_szelesseg, kep_magassag])


def cica(bal_kezd=0, felso_kezd=0, meret_szorzo=0.4):
    def elipszis(x, y, szelesseg, magassag, szin):
        return pygame.draw.ellipse(screen, szin,
                                   (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                                    szelesseg * meret_szorzo, magassag * meret_szorzo))

    def koriv(x, y, szelesseg, magassag, a1, a2, szin):
        return pygame.draw.arc(screen, szin,
                               (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                                szelesseg * meret_szorzo, magassag * meret_szorzo)
                               , a1, a2)

    def vonal(x1, y1, x2, y2, vastagsag, szin):
        return pygame.draw.line(screen, szin,
                                (dx + bal_kezd + x1 * meret_szorzo, dy + felso_kezd + y1 * meret_szorzo),
                                (dx + bal_kezd + x2 * meret_szorzo, dy + felso_kezd + y2 * meret_szorzo),
                                vastagsag)

    def fej():
        fej = elipszis(10, 20, 110, 85, black)
        fej.union(elipszis(20, 0, 33, 85, black))
        fej.union(elipszis(76, 0, 33, 85, black))
        return fej

    def szem():
        szem = koriv(30, 40, 30, 10, 0, 3.2, white)
        szem.union(koriv(30, 40, 30, 10, 0, 6.4, white))
        szem.union(koriv(75, 40, 30, 10, 0, 3.2, white))
        szem.union(koriv(75, 40, 30, 10, 0, 6.4, white))
        szem.union(elipszis(41, 40, 7, 10, white))
        szem.union(elipszis(86, 40, 7, 10, white))
        return szem

    def orr_es_szaj():
        orr = elipszis(64, 65, 10, 8, white)
        orr.union(vonal(67, 65, 67, 81, 2, white))
        orr.union(koriv(59, 81, 20, 20, 0, 3.14, white))
        return orr

    def bajusz():
        bajusz = vonal(10, 66, 120, 66, 2, white)
        bajusz.union(vonal(0, 66, 10, 66, 2, black))
        bajusz.union(vonal(120, 66, 130, 66, 2, black))
        bajusz.union(vonal(10, 59, 120, 71, 2, white))
        bajusz.union(vonal(0, 59, 10, 60, 2, black))
        bajusz.union(vonal(120, 70, 130, 71, 2, black))
        bajusz.union(vonal(10, 71, 120, 59, 2, white))
        bajusz.union(vonal(0, 71, 10, 70, 2, black))
        bajusz.union(vonal(120, 60, 130, 59, 2, black))
        return bajusz

    cica = fej()
    cica.union(szem())
    cica.union(bajusz())
    cica.union(orr_es_szaj())
    return cica


running = True
is_mouse_down = False
dx = 0
dy = 0
pressed_keys = {}

while running:

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            pressed_keys[event.key] = True
        elif event.type == pygame.KEYUP:
            pressed_keys[event.key] = False
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False

    for key in pressed_keys:
        if pressed_keys[key]:
            if key == pygame.K_UP:
                if dy - 1 >= 0:
                    dy -= 1
            elif key == pygame.K_DOWN:
                if dy + 1 <= 500:
                    dy += 1
            elif key == pygame.K_LEFT:
                if dx - 1 >= 0:
                    dx -= 1
            elif key == pygame.K_RIGHT:
                if dx + 1 <= 500:
                    dx += 1

    screen.fill(white)  # RGB ertek, feher

    cicc = cica(0, 0)


    def labirintus():

        def labirintus_vonal(p1, p2):
            global dx, dy
            v = pygame.draw.line(screen, black, p1, p2, 1)
            if v.colliderect(cicc):
                dx = 0
                dy = 0

        labirintus_vonal((50, 50), (50, 200))  # függőleges
        labirintus_vonal((100, 0), (100, 150))  # függőleges
        labirintus_vonal((50, 200), (150, 200))  # vízszintes
        labirintus_vonal((100, 100), (350, 100))  # vízszintes
        labirintus_vonal((150, 50), (450, 50))  # vízszintes
        labirintus_vonal((150, 150), (300, 150))  # vízszintes
        labirintus_vonal((450, 50), (450, 100))  # függőleges
        labirintus_vonal((400, 50), (400, 150))  # függőleges
        labirintus_vonal((400, 150), (500, 150))  # vízszintes
        labirintus_vonal((350, 100), (350, 200))  # függőleges
        labirintus_vonal((0, 250), (200, 250))  # vízszintes
        labirintus_vonal((200, 200), (250, 200))  # vízszintes
        labirintus_vonal((350, 100), (350, 200))  # függőleges
        labirintus_vonal((200, 200), (200, 250))  # függőleges
        labirintus_vonal((250, 200), (250, 250))  # függőleges
        labirintus_vonal((150, 150), (150, 200))  # függőleges
        labirintus_vonal((300, 150), (300, 250))  # függőleges
        labirintus_vonal((350, 200), (450, 200))  # vízszintes
        labirintus_vonal((300, 250), (400, 250))  # vízszintes
        labirintus_vonal((50, 300), (50, 400))  # függőleges
        labirintus_vonal((100, 250), (100, 450))  # függőleges
        labirintus_vonal((50, 450), (100, 450))  # vízszintes
        labirintus_vonal((400, 250), (400, 300))  # függőleges
        labirintus_vonal((450, 250), (450, 400))  # függőleges
        labirintus_vonal((0, 400), (50, 400))  # vízszintes
        labirintus_vonal((150, 300), (400, 300))  # vízszintes
        labirintus_vonal((150, 300), (150, 500))  # függőleges
        labirintus_vonal((200, 350), (200, 450))  # függőleges
        labirintus_vonal((200, 350), (450, 350))  # vízszintes
        labirintus_vonal((250, 400), (350, 400))  # vízszintes
        labirintus_vonal((200, 450), (300, 450))  # vízszintes
        labirintus_vonal((400, 450), (500, 450))  # vízszintes
        labirintus_vonal((350, 400), (350, 500))  # függőleges
        labirintus_vonal((400, 350), (400, 450))  # függőleges
        pygame.draw.line(screen, red, (450, 450), (500, 500), 5)
        pygame.draw.line(screen, red, (500, 450), (450, 500), 5)


    labirintus()

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"
    clock.tick(100)

# Done! Time to quit.
pygame.quit()
