def fmondat(mondat_param):
    szavak = mondat_param.split(" ")
    return szavak


mondat = input("Irj be egy mondatot: ")
szavak = fmondat(mondat)
eredmeny = " ".join(reversed(szavak))
print(mondat, "<->", eredmeny)


def alternativa():
    mondat = input("Irj be egy mondatot: ")
    szavak = mondat.split(" ")
    forditott_mondat = ""
    for szo in reversed(szavak):
        forditott_mondat += szo + " "

    print(forditott_mondat)
