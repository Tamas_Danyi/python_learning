# 22. Írj egy Python programot, amely bekér a felhasználótól egy sztringet és ezt úgy íratja ki, hogy
# a szóköz karaktereket kihagyja!
mondat = "Irj be egy mondatot"
eredmeny = mondat.replace(" ", "")
eredmeny2 = "".join(mondat.split(" "))

print(eredmeny, eredmeny2)
