# 23. Írj egy Python programot, amely a
# felhasználótól pozitív számokat kér be
# mindaddig, amíg afelhasználó nullát nem ad be!
# A program az összes értéket tárolja egy listában,
# majd írja ki a képernyőre a lista elemeit
# fordított sorrendben!
import numbers

pozitiv_szamok = []
szam = int(input("Adj meg egy szamot: "))

while szam != 0:
    if szam > 0:
        pozitiv_szamok.append(szam)
    szam = int(input("Adj meg megint egy szamot: "))

print(list(reversed(pozitiv_szamok)))

#
# def pozitiv_beker():
#     return int(input("Adj meg egy szamot: "))
#
#
# pozitiv_szamok = []
# szam = pozitiv_beker()
#
# while szam != 0:
#     if szam > 0:
#         pozitiv_szamok.append(szam)
#     szam = pozitiv_beker()
#
# print(list(reversed(pozitiv_szamok)))
