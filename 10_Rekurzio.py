def vegtelen_fgv():
    print("meghivtuk a fuggvenyt")
    a = 5
    vegtelen_fgv()  # a kovetkezo sort soha nem eri el a futas
    print("visszaterunk a fuggvenybol a kovetkezo ertekkel:", a)
    return a


# print(f"eredmeny: {vegtelen_fgv()}")


# 1. legyen kilepesi/megallasi feltetel
# 2. matematikai keplettel kiszamolhato legyen az n-dik hivas eredmenye korabbi hivasokbol

def faktorialis_rekurziv(n):
    # n! = n * (n-1) * (n-2) * ... * 2 * 1 = n * (n-1)!
    # pl 5! = 5*4*3*2*1 = 120
    if n <= 1:  # mintha ezt fejbol kiszamolnank, mert trivialis
        return 1
    return n * faktorialis_rekurziv(n - 1)


print(faktorialis_rekurziv(5))


def faktorialis_loop(n):
    fakt = 1
    while n > 0:
        fakt = fakt * n
        n = n - 1
    return fakt


print(faktorialis_loop(5))


# pelda: Fibonacci-sorozat: 0,1,1,2,3,5,8,13,...,n-2.elem,n-1.elem,n.elem
def fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


for i in range(15):
    print(fibonacci(i), end=" ")
print()


# 0,1,2,3,y,x,6,7,8,9,...,n
def sorozat1(n):
    if n == 0:
        return 0
    return sorozat1(n - 1) + 1


for i in range(10):
    print(sorozat1(i), end=" ")
print()


# 1,4, 13, ... , f(n) = f(n-1)*3 + 1
def sorozat2(n):
    if n == 0:
        return 0
    return sorozat2(n - 1) * 3 + 1


for i in range(10):
    print(sorozat2(i), end=" ")
print()
