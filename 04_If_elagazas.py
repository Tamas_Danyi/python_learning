# if <logikai_kifejezes>:
# <behúzás> a=1 # blokk eleje
# <behúzás> b=2
# <behúzás> c=3 # blokk vége; addig tart a blokk, amíg a behúzás ugyanakkora, mint előtte
# ################################

if True:
    print("kiirja")
if False:
    print("nem irja ki")

print("################################")

logikai_ertek = True
if logikai_ertek:
    print("ezt is kiirja")
if 4 < 5:
    print("4 kisebb 5-nel")
if 6 < 5:
    print("ulj le fiam, egyes")

print("################################")

a = 4
b = 4
if a < b:
    print("a < b")
else:
    print("a >= b")

print("################################")

a = 41
b = 7
if a < b:
    print("a < b")
elif b < a:
    print("b < a")
else:
    print("a == b")

print("################################")

kor = int(input("hany eves vagy? "))
if kor < 16:
    print("nem nezheted meg Berki Mazsi szuleset")
elif 16 <= kor and kor < 18:
    print("megnezheted, de nem ihatsz sort")
    print("sem palinkat")
    print("csak vizet")
elif kor < 20:
    print("szuleid kuldjenek penzt szegeny Krisztiannak")
else:
    print("kuldj penzt szegeny Krisztiannak")

print("################################")

if True:
    pass  # enelkul nem futna le
print("hello")

print("################################")

answer = input("(i)gaz vagy (h)amis? 6<7 ")
if answer == 'i':
    print("Gratulalok, te igazan okos vagy")
elif answer == 'h':
    print("Sajnos most nem nyertel, Cucu malac")
else:
    print("tanulj meg olvasni / irni")

print("################################")

kor = int(input("hany eves vagy? "))
if kor < 19 or kor > 78:
    print("jo etvagyat a tejbepapihoz")
else:
    print("menj dolgozni")

print("################################")

print("## az előző megoldása egymásba ágyazott elágazással ##")
kor = int(input("hany eves vagy? "))
if kor <= 78:
    # külső blokk eleje
    if kor < 19:
        print("jo etvagyat a tejbepapihoz, kis pisis")  # belső blokk 1
    else:
        print("menj dolgozni")  # belső blokk 2
    # külső blokk vége
else:
    print("jo etvagyat a tejbepapihoz")

print("################################")

# Feladatok a hétvégére
########################
# 1. Kérj be a felhasználótól 3 egész számot, és döntsd el, hogy ilyen hosszú oldalakkal létezik-e háromszög!
# (Akkor és csak akkor létezik ilyen háromszög, ha bármely két szám összege nagyobb, mint a harmadik szám. Tehát a+b>c, a+c>b és b+c>a.)
#
# 2. Azt is határozd meg, ha ez a háromszög szabályos háromszög-e, egyenlő szárú háromszög-e, derékszögű háromszög-e?
