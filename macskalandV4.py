import random

import pygame

clock = pygame.time.Clock()
pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

kep_szelesseg = 500
kep_magassag = 500
screen = pygame.display.set_mode([kep_szelesseg, kep_magassag])
step = 1
running = True
is_mouse_down = False
dx = 0
dy = 0
pressed_keys = {}
# mozgasi lehetosegeket tarolo valtozok
mehet_fel = True
mehet_le = True
mehet_balra = True
mehet_jobbra = True
# targyak amikkel utkozhet a cica
cseresznyek = []


def print_rect(rect, name="rect"):
    print(name, "balfelso:" + str(rect.topleft) + " - jobbalso:" + str(rect.bottomright), rect)


def cica(bal_kezd=0, felso_kezd=0, meret_szorzo=0.4):
    def elipszis(x, y, szelesseg, magassag, szin):
        return pygame.draw.ellipse(screen, szin,
                                   (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                                    szelesseg * meret_szorzo, magassag * meret_szorzo))

    def koriv(x, y, szelesseg, magassag, a1, a2, szin):
        return pygame.draw.arc(screen, szin,
                               (dx + bal_kezd + x * meret_szorzo, dy + felso_kezd + y * meret_szorzo,
                                szelesseg * meret_szorzo, magassag * meret_szorzo)
                               , a1, a2)

    def vonal(x1, y1, x2, y2, vastagsag, szin):
        return pygame.draw.line(screen, szin,
                                (dx + bal_kezd + x1 * meret_szorzo, dy + felso_kezd + y1 * meret_szorzo),
                                (dx + bal_kezd + x2 * meret_szorzo, dy + felso_kezd + y2 * meret_szorzo),
                                vastagsag)

    def fej():
        fej = elipszis(10, 20, 110, 85, black)
        fej.union(elipszis(20, 0, 33, 85, black))
        fej.union(elipszis(76, 0, 33, 85, black))
        return fej

    def szem():
        szem = koriv(30, 40, 30, 10, 0, 3.2, white)
        szem.union(koriv(30, 40, 30, 10, 0, 6.4, white))
        szem.union(koriv(75, 40, 30, 10, 0, 3.2, white))
        szem.union(koriv(75, 40, 30, 10, 0, 6.4, white))
        szem.union(elipszis(41, 40, 7, 10, white))
        szem.union(elipszis(86, 40, 7, 10, white))
        return szem

    def orr_es_szaj():
        orr = elipszis(64, 65, 10, 8, white)
        orr.union(vonal(67, 65, 67, 81, 2, white))
        orr.union(koriv(59, 81, 20, 20, 0, 3.14, white))
        return orr

    def bajusz():
        bajusz = vonal(10, 66, 120, 66, 2, white)
        bajusz.union(vonal(0, 66, 10, 66, 2, black))
        bajusz.union(vonal(120, 66, 130, 66, 2, black))
        bajusz.union(vonal(10, 59, 120, 71, 2, white))
        bajusz.union(vonal(0, 59, 10, 60, 2, black))
        bajusz.union(vonal(120, 70, 130, 71, 2, black))
        bajusz.union(vonal(10, 71, 120, 59, 2, white))
        bajusz.union(vonal(0, 71, 10, 70, 2, black))
        bajusz.union(vonal(120, 60, 130, 59, 2, black))
        return bajusz

    cica = fej()
    cica.union(szem())
    cica.union(bajusz())
    cica.union(orr_es_szaj())
    return cica


def event_handling():
    global running
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            pressed_keys[event.key] = True
        elif event.type == pygame.KEYUP:
            pressed_keys[event.key] = False
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False


def key_handling():
    global dx, dy
    for key in pressed_keys:
        if pressed_keys[key]:
            if key == pygame.K_UP:
                if dy - step >= 0 and mehet_fel:
                    dy -= step
            elif key == pygame.K_DOWN:
                if dy + step <= 460 and mehet_le:
                    dy += step
            elif key == pygame.K_LEFT:
                if dx - step >= 0 and mehet_balra:
                    dx -= step
            elif key == pygame.K_RIGHT:
                if dx + step <= 450 and mehet_jobbra:
                    dx += step


def labirintus(cico):
    global mehet_fel, mehet_le, mehet_jobbra, mehet_balra

    def labirintus_vonal(p1, p2):
        global dx, dy, mehet_fel, mehet_le, mehet_jobbra, mehet_balra
        v = pygame.draw.line(screen, black, p1, p2, 1)

        # ha utkozik a macska a fallal, akkor letiltjuk a megfelelo mozgasi lehetoseget
        if v.colliderect(cico):
            if abs(v.right - cico.left) <= step:
                mehet_balra = False
            if abs(v.top - cico.bottom) <= step:
                mehet_le = False
            if abs(v.bottom - cico.top) <= step:
                mehet_fel = False
            if abs(v.left - cico.right) <= step:
                mehet_jobbra = False

    labirintus_vonal((50, 50), (50, 200))  # függőleges
    labirintus_vonal((100, 0), (100, 150))  # függőleges
    labirintus_vonal((50, 200), (150, 200))  # vízszintes
    labirintus_vonal((100, 100), (350, 100))  # vízszintes
    labirintus_vonal((150, 50), (450, 50))  # vízszintes
    labirintus_vonal((150, 150), (300, 150))  # vízszintes
    labirintus_vonal((450, 50), (450, 100))  # függőleges
    labirintus_vonal((400, 50), (400, 150))  # függőleges
    labirintus_vonal((400, 150), (500, 150))  # vízszintes
    labirintus_vonal((350, 100), (350, 200))  # függőleges
    labirintus_vonal((0, 250), (200, 250))  # vízszintes
    labirintus_vonal((200, 200), (250, 200))  # vízszintes
    labirintus_vonal((350, 100), (350, 200))  # függőleges
    labirintus_vonal((200, 200), (200, 250))  # függőleges
    labirintus_vonal((250, 200), (250, 250))  # függőleges
    labirintus_vonal((150, 150), (150, 200))  # függőleges
    labirintus_vonal((300, 150), (300, 250))  # függőleges
    labirintus_vonal((350, 200), (450, 200))  # vízszintes
    labirintus_vonal((300, 250), (400, 250))  # vízszintes
    labirintus_vonal((50, 300), (50, 400))  # függőleges
    labirintus_vonal((100, 250), (100, 450))  # függőleges
    labirintus_vonal((50, 450), (100, 450))  # vízszintes
    labirintus_vonal((400, 250), (400, 300))  # függőleges
    labirintus_vonal((450, 250), (450, 400))  # függőleges
    labirintus_vonal((0, 400), (50, 400))  # vízszintes
    labirintus_vonal((150, 300), (400, 300))  # vízszintes
    labirintus_vonal((150, 300), (150, 500))  # függőleges
    labirintus_vonal((200, 350), (200, 450))  # függőleges
    labirintus_vonal((200, 350), (450, 350))  # vízszintes
    labirintus_vonal((250, 400), (350, 400))  # vízszintes
    labirintus_vonal((200, 450), (300, 450))  # vízszintes
    labirintus_vonal((400, 450), (500, 450))  # vízszintes
    labirintus_vonal((350, 400), (350, 500))  # függőleges
    labirintus_vonal((400, 350), (400, 450))  # függőleges
    pygame.draw.line(screen, red, (450, 450), (500, 500), 5)
    pygame.draw.line(screen, red, (500, 450), (450, 500), 5)


def cseresznye(x, y, cico):
    global cseresznyek
    cs = pygame.Rect(x, y, 5, 5)
    print("uj cseresznye: ", x, y)
    return cs


iteracio_szamlalo = 0
while running:
    # minden ciklus elejen bealitjuk az alapertelmezett mozgasi ertekeket, amiket majd a 'labirintus_vonal' funkcioban modositunk
    mehet_jobbra = True
    mehet_balra = True
    mehet_fel = True
    mehet_le = True

    # while len(pressed_keys.keys()) == 0:
    event_handling()

    screen.fill(white)  # RGB ertek, feher

    cicc = cica(0, 0)

    if iteracio_szamlalo % 500 == 0:
        cseresznyek.append(cseresznye(random.randint(0, 500), random.randint(0, 500), cicc))

    for cs in cseresznyek:
        if cicc.colliderect(cs):
            cseresznyek.remove(cs)
            print("cseresznye torolve: ", x, y)
        else:
            pygame.draw.rect(screen, red, cs)

    labirintus(cicc)

    key_handling()

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

    clock.tick(100)
    iteracio_szamlalo += 1

# Done! Time to quit.
pygame.quit()
