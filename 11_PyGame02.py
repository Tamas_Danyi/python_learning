import pygame

pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

screen = pygame.display.set_mode([800, 600])

running = True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False

    screen.fill(white)  # RGB ertek, feher

    # Draw a solid blue circle in the center
    # pygame.draw.circle(screen, red, (250, 250), 150)
    pygame.draw.rect(screen, green, (10, 10, 100, 20))
    pygame.draw.ellipse(screen, blue, (10, 50, 100, 20))
    pygame.draw.arc(screen, black, (150, 150, 100, 80), 2, 2 + 3.14)
    pygame.draw.line(screen, red, (50, 50), (150, 150), 3)

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

# Done! Time to quit.
pygame.quit()
