print("Aritmetikai operátorok")
print(" +, -, *, /, %, //, **")
print("\n")
print(3 + 2)  # szám+szám vagy string+string (konkatenáció -- concatenate)
print(3 - 2)
print(3 * 2)
print(3 / 2)  # eredménye float
print(77 % 13)
print(77 // 13)  # eredménye int
print(3 ** 2)
print(12 + 5 * 13)

print("\n\n\n")
print("Összehasonlító operátorok")
print(" ==, !=, >, <, >=, <= ")
print("\n")
print(4 == 4)  # True
print(4 != 4)  # False
print(4 < 4)  # False
print(4 <= 4)  # True
print(4 > 4)  # False
print(4 >= 4)  # True
print("\n")
print(4 == 5)  # False
print(4 != 5)  # True
print(4 < 5)  # True
print(4 <= 5)  # True
print(4 > 5)  # False
print(4 >= 5)  # False

print("\n\n\n")
print("Logikai operátorok")
print(" and, or, not ")
print(True and False)  # False
print(True or False)  # True
print(not False)  # True
print("\n")

print("\n\n\n")
print("Hozzárendelő operátorok")
print(" =, +=, -=, *=, /=, stb")
valtozo_nev = 5
valtozo_nev += 1
print(valtozo_nev)  # 6
valtozo_nev -= 2
print(valtozo_nev)  # 4
valtozo_nev /= 2
print(valtozo_nev)  # 2
valtozo_nev *= 3
print(valtozo_nev)  # 6
