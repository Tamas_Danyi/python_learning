# from dt_util import pi_happy  # közvetlen importálás
import random

import dt_util  # egész modul/fájl importálása

print(dt_util.pi_happy)  # importált modul felhasználása
print(dt_util.pi_chai)  # importált modul felhasználása

# print(pi_happy) # közvetlen importálás felhasználása
# print(pi_chai) # nem importáltuk, ezért hibás

print(random.random())
print(random.randint(1, 5))

# # 1.1 Feladat
# # Készíts egy programot, amely megkérdezi a felhasználótól, hogy jó napja van-e!
# # A válasz kétféle lehet: igen vagy nem.  A választól függően írjon ki üzenetet a gép!
#
# valasz = input("Szia, Felhasználó! Jó napod van? (igen/nem)")
# if valasz == "igen":
#     print("Az jó!")
# else:
#     print("Nagyon sajnálom!")
#
# # 1.2 Feladat
# # Fejleszd tovább az első feladat programját úgy, hogy amennyiben a felhasználó nem a két lehetséges válasz (igen/nem) közül ad meg egyet, a gép kiírja: "Sajnos nem értem a válaszodat!"
# valasz = input("Szia, Felhasználó! Jó napod van? (igen/nem)")
# if valasz == "igen":
#     print("Az jó!")
# elif valasz == "nem":
#     print("Nagyon sajnálom!")
# else:
#     print("Sajnos nem értem a válaszodat!")
#
# # 2. Feladat
# # Készíts egy programot, ami bekér egy számot a felhasználótól, majd kiírja, hogy a megadott szám páros-e vagy páratlan!
# # [Tipp] A maradékos osztás segít! Mennyivel is kell elosztanod a számot maradékosan, hogy kiderüljön páros-e? Ebben az esetben mennyi lesz a maradék?
# szam = int(input("Adj meg egy számot: "))
# if szam % 2 == 0:
#     print(f"Formázott stringgel kiíratom, hogy páros.")
# else:
#     print(f"Páratlan szám ez a {szam}!")

# 3. Feladat
# Készíts egy programot! A gép "gondol" egy számra 1 és 5 között, vagyis egy változóban tárolj egy ilyen számot! Azután a program bekér egy számot a felhasználótól, majd kiírja, hogy ez a szám egyenlő-e a gép által "gondolt" számmal, vagy annál kisebb, illetve nagyobb.
gondolt_szam = random.randint(1, 5)
bekert_szam = int(input("Melyik számra gondoltam? 1-től 5-ig: "))
if gondolt_szam == bekert_szam:
    print("Talált!")
else:
    if gondolt_szam > bekert_szam:
        nagyobb = "nagyobb"
    else:
        nagyobb = "kisebb"
    print(f"Nem talált. Az általam gondolt szám a  {gondolt_szam}, ami {nagyobb}")
    # ne hagyd magad összezavarni! a 'nagyobb' itt egy változónév, aminek lehet az értéke "nagyobb" vagy "kisebb"

# 4. Feladat
# Készíts egy programot, amely a felhasználótól bekér egy egész számot, majd megvizsgálja, hogy a megadott szám
# - pozitív páros vagy
# - negatív páros vagy
# - negatív páratlan.
# - pozitív páratlan.
# Az eredményről tájékoztatja a felhasználót.
szam4 = int(input("adjon meg egy szamot: "))
paros = szam4 % 2 == 0
eredmeny = ''
if szam4 < 0:
    eredmeny += "negativ "
elif szam4 > 0:
    eredmeny += "pozitiv "
if paros:
    eredmeny += "paros"
else:
    eredmeny += "paratlan"
print(eredmeny)
#
# 5. Feladat
# Készíts egy programot, amely a felhasználótól két külön kérdésben megkérdezi, hogy az ikrek (Henrik és Hanna) jönnek-e ma kosrazni! Például így: Jön Henrik ma kosarazni? (igen/nem). A program írja ki, hogy melyik állítás igaz az alábbiak közül:
# - egyikük sem jön kosarazni,
# - mind a ketten jönnek kosarazni,
# - csak az egyikük jön kosarazni.
jon_hanna = input("Hanna jon kosarazni? ") == "igen"
jon_henrik = input("Henrik jon kosarazni? ") == "igen"
if jon_henrik and jon_hanna:
    print("mind a ketten jönnek kosarazni")
elif jon_henrik or jon_hanna:
    print("csak az egyikük jön kosarazni")
else:
    print("egyikük sem jön kosarazni")

#
# 6. Feladat
# Készíts egy programot, amely a felhasználó által megadott egész számról eldönti, hogy
# - csak 3-mal osztható,
# - csak 4-gyel osztható,
# - 3-mal és 4-gyel is osztható,
# - sem 3-mal, sem 4-gyel nem osztható!
szam6 = int(input("adjon meg egy szamot: "))
neggyel_oszthato = szam6 % 4 == 0  # igy csak egyszer fut le es eltaroljuk az eredmenyt
harommal_oszthato = szam6 % 3 == 0  # igazabol egyszer eleg is lefuttatni
if neggyel_oszthato and harommal_oszthato:
    print("3-mal és 4-gyel is osztható")
elif neggyel_oszthato:
    print("csak 4-gyel osztható")
elif harommal_oszthato:
    print("csak 3-mal osztható")
else:
    print("sem 3-mal, sem 4-gyel nem osztható!")
