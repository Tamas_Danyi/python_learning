import pygame

pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)


screen = pygame.display.set_mode([800, 600])


def cica(bal_kezd=0, felso_kezd=0):
    pygame.draw.ellipse(screen, black, (bal_kezd + 10, felso_kezd + 20, 110, 85))
    pygame.draw.ellipse(screen, black, (bal_kezd + 20, felso_kezd + 0, 33, 85))
    pygame.draw.ellipse(screen, black, (bal_kezd + 76, felso_kezd + 0, 33, 85))
    pygame.draw.ellipse(screen, white, (bal_kezd + 41, felso_kezd + 40, 7, 10))
    pygame.draw.ellipse(screen, white, (bal_kezd + 86, felso_kezd + 40, 7, 10))
    pygame.draw.arc(screen, white, (bal_kezd + 30, felso_kezd + 40, 30, 10), 0, 3.2)
    pygame.draw.arc(screen, white, (bal_kezd + 30, felso_kezd + 40, 30, 10), 0, 6.4)
    pygame.draw.arc(screen, white, (bal_kezd + 75, felso_kezd + 40, 30, 10), 0, 3.2)
    pygame.draw.arc(screen, white, (bal_kezd + 75, felso_kezd + 40, 30, 10), 0, 6.4)
    pygame.draw.ellipse(screen, white, (bal_kezd + 64, felso_kezd + 65, 10, 8))
    pygame.draw.line(screen, white, (bal_kezd + 67, felso_kezd + 65), (bal_kezd + 67, felso_kezd + 81), 2)
    pygame.draw.arc(screen, white, (bal_kezd + 59, felso_kezd + 81, 20, 20), 0, 3.14)
    pygame.draw.line(screen, white, (bal_kezd + 10, felso_kezd + 66), (bal_kezd + 120, felso_kezd + 66), 2)
    pygame.draw.line(screen, black, (bal_kezd + 0, felso_kezd + 66), (bal_kezd + 10, felso_kezd + 66), 2)
    pygame.draw.line(screen, black, (bal_kezd + 120, felso_kezd + 66), (bal_kezd + 130, felso_kezd + 66), 2)
    pygame.draw.line(screen, white, (bal_kezd + 10, felso_kezd + 59), (bal_kezd + 120, felso_kezd + 71), 2)
    pygame.draw.line(screen, black, (bal_kezd + 0, felso_kezd + 59), (bal_kezd + 10, felso_kezd + 60), 2)
    pygame.draw.line(screen, black, (bal_kezd + 120, felso_kezd + 70), (bal_kezd + 130, felso_kezd + 71), 2)
    pygame.draw.line(screen, white, (bal_kezd + 10, felso_kezd + 71), (bal_kezd + 120, felso_kezd + 59), 2)
    pygame.draw.line(screen, black, (bal_kezd + 0, felso_kezd + 71), (bal_kezd + 10, felso_kezd + 70), 2)
    pygame.draw.line(screen, black, (bal_kezd + 120, felso_kezd + 60), (bal_kezd + 130, felso_kezd + 59), 2)
    print("cica")


running = True
while running:

    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False

    screen.fill(white)  # RGB ertek, feher

    for x in range(0, 600, 150):
        for y in range(0, 600, 150):
            cica(x, y)

    print("===")

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

# Done! Time to quit.
pygame.quit()
