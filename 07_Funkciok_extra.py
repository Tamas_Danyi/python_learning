print("#### funkcio alapertelmezett/default parameter ertekekkel")


def funkcio(alap, exp=1):
    return alap ** exp


print("funkcio", funkcio(1))  # 1
print("funkcio", funkcio(2))  # 2
print("funkcio", funkcio(2, 1))  # 2, az alapertekkel megegyezo parameter
print("funkcio", funkcio(2, 2))  # 4

print("#### funkcio hivasa megnevezett parameterekkel")


def funkcio2(alap, exp):
    return alap ** exp


print(funkcio2(10, 2))  # 100
print(funkcio2(2, 10))  # 1024
print(funkcio2(exp=10, alap=2))  # 1024, a valtozok neveit meg kell adni
print(funkcio2(2, exp=3))  # 8, sorrendben is van es a valtozo neve is passzol, ezert ez jo

# print(funkcio(alap=3, 2))  # SyntaxError, ha az elso parameternek van neve, a tobbinek is meg kell adni


print("#### kerdesek")


def kerdes(a=2, b=3):
    return a * b


print(kerdes())
print(kerdes(1))
print(kerdes(4))
print(kerdes(1, 4))
print(kerdes(4, 1))
print(kerdes(a=5))
print(kerdes(b=5))
