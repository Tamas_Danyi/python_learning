import numpy as np

# 3.1 Lesson
heights = list(range(45))
heights_arr = np.array(heights)
print((heights_arr % 3 == 0).sum())
print(heights_arr.size, heights_arr.shape)
#
ages = list(range(100, 145))
ages_arr = np.array(ages)
heights_and_ages = heights + ages
heights_and_ages_arr = np.array(heights_and_ages)
print(heights_and_ages_arr.size, heights_and_ages_arr.shape)
heights_and_ages_arr = heights_and_ages_arr.reshape(2, int(heights_and_ages_arr.size / 2))
print(heights_and_ages_arr.size, heights_and_ages_arr.shape)
#
print(heights_arr.dtype)
#
heights_arr[3] = 165
heights_and_ages_arr[1, 3] = 165
# 5.1 Lesson
heights_and_ages_arr[0, :] = 180
heights_and_ages_arr[:2, :2] = 0
#
heights_and_ages_arr[:, 0] = [190, 58]
new_record = np.array([[180, 183, 190], [54, 50, 69]])
heights_and_ages_arr[:, 42:] = new_record
#
print("333333333333")
# print(heights_arr, heights_arr.shape)
ages_arr = ages_arr.reshape((45, 1))
heights_arr = heights_arr.reshape((45, 1))
heights_age_arr = np.hstack((heights_arr, ages_arr))
# print(heights_age_arr[:3, ], heights_age_arr.shape)
# ages_arr = ages_arr.reshape((1, 45))
# heights_arr = heights_arr.reshape((1, 45))
# heights_age_arr = np.vstack((heights_arr, ages_arr))
# print(heights_age_arr[:, :3], heights_age_arr.shape)
#
# print("a", heights_age_arr[:, :3])
# heights_age_arr_axis1 = np.concatenate((heights_arr[:, :3], ages_arr[:, :3]), axis=1)
# heights_age_arr_axis0 = np.concatenate((heights_arr[:, :3], ages_arr[:, :3]), axis=0)
# print("b", heights_age_arr_axis1)
# print("c", heights_age_arr_axis0)
#
# # 6.1 Lesson
# print(heights_age_arr[:, 0] * 0.0328084)
# #
# print((heights_age_arr[:2, :3]).sum())
# print((heights_age_arr[:2, :3]).sum(axis=0))
# print((heights_age_arr[:2, :3]).sum(axis=1))
#
print("heights_age_arr", heights_age_arr)
mask = heights_age_arr[:, 0] % 3 == 0
print("mask", mask)
dividable_by_3 = heights_age_arr[mask,]
print("div:", dividable_by_3)
