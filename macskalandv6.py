import random

import pygame

ora = pygame.time.Clock()
pygame.init()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
black = (0, 0, 0)

kep_szelesseg = 500
kep_magassag = 500
kepernyo = pygame.display.set_mode([kep_szelesseg, kep_magassag])
cica_lepes = 1
program_fut = True
cica_x = 0
cica_y = 0
lenyomott_bill = {}
# mozgasi lehetosegeket tarolo valtozok

mehet_fel = True
mehet_le = True
mehet_balra = True
mehet_jobbra = True
# targyak amikkel utkozhet a cica
cseresznye = None


def uj_eger():
    eger = pygame.image.load('mice.png')
    eger = pygame.transform.scale(eger, (50, 50))
    return eger


def cica(bal_kezd=0, felso_kezd=0, meret_szorzo=0.37, labirintus=None):
    global mehet_fel, mehet_le, mehet_jobbra, mehet_balra

    def elipszis(x, y, szelesseg, magassag, szin):
        return pygame.draw.ellipse(kepernyo, szin,
                                   (cica_x + bal_kezd + x * meret_szorzo, cica_y + felso_kezd + y * meret_szorzo,
                                    szelesseg * meret_szorzo, magassag * meret_szorzo))

    def koriv(x, y, szelesseg, magassag, a1, a2, szin):
        return pygame.draw.arc(kepernyo, szin,
                               (cica_x + bal_kezd + x * meret_szorzo, cica_y + felso_kezd + y * meret_szorzo,
                                szelesseg * meret_szorzo, magassag * meret_szorzo)
                               , a1, a2)

    def vonal(x1, y1, x2, y2, vastagsag, szin):
        return pygame.draw.line(kepernyo, szin,
                                (cica_x + bal_kezd + x1 * meret_szorzo, cica_y + felso_kezd + y1 * meret_szorzo),
                                (cica_x + bal_kezd + x2 * meret_szorzo, cica_y + felso_kezd + y2 * meret_szorzo),
                                vastagsag)

    def fej():
        fej = elipszis(10, 20, 110, 85, black)
        fej.union(elipszis(20, 0, 33, 85, black))
        fej.union(elipszis(76, 0, 33, 85, black))
        return fej

    def szem():
        szem = koriv(30, 40, 30, 10, 0, 3.2, white)
        szem.union(koriv(30, 40, 30, 10, 0, 6.4, white))
        szem.union(koriv(75, 40, 30, 10, 0, 3.2, white))
        szem.union(koriv(75, 40, 30, 10, 0, 6.4, white))
        szem.union(elipszis(41, 40, 7, 10, white))
        szem.union(elipszis(86, 40, 7, 10, white))
        return szem

    def orr_es_szaj():
        orr = elipszis(64, 65, 10, 8, white)
        orr.union(vonal(67, 65, 67, 81, 2, white))
        orr.union(koriv(59, 81, 20, 20, 0, 3.14, white))
        return orr

    def bajusz():
        bajusz = vonal(10, 66, 120, 66, 2, white)
        bajusz.union(vonal(0, 66, 10, 66, 2, black))
        bajusz.union(vonal(120, 66, 130, 66, 2, black))
        bajusz.union(vonal(10, 59, 120, 71, 2, white))
        bajusz.union(vonal(0, 59, 10, 60, 2, black))
        bajusz.union(vonal(120, 70, 130, 71, 2, black))
        bajusz.union(vonal(10, 71, 120, 59, 2, white))
        bajusz.union(vonal(0, 71, 10, 70, 2, black))
        bajusz.union(vonal(120, 60, 130, 59, 2, black))
        return bajusz

    cica = fej()
    cica.union(szem())
    cica.union(bajusz())
    cica.union(orr_es_szaj())

    # ha utkozik a macska a fallal, akkor letiltjuk a megfelelo mozgasi lehetoseget
    for v in labirintus:
        utkozhet_fuggolegesen = v.top < cica.bottom and cica.top < v.bottom  # egyebkent elmenne mellette
        if utkozhet_fuggolegesen:
            if abs(cica.left - v.right) < cica_lepes:
                mehet_balra = False
            if abs(v.left - cica.right) < cica_lepes:
                mehet_jobbra = False

        utkozhet_vizszintesen = v.left < cica.right and cica.left < v.right  # egyebkent elmenne mellette
        if utkozhet_vizszintesen:
            if abs(v.bottom - cica.top) < cica_lepes:
                mehet_fel = False
            if abs(cica.bottom - v.top) < cica_lepes:
                mehet_le = False

    return cica


def esemeny_kezeles():
    global program_fut
    for esemeny in pygame.event.get():
        if esemeny.type == pygame.KEYDOWN:
            lenyomott_bill[esemeny.key] = True
        elif esemeny.type == pygame.KEYUP:
            lenyomott_bill[esemeny.key] = False
        if esemeny.type == pygame.QUIT or (esemeny.type == pygame.KEYDOWN and esemeny.key == pygame.K_ESCAPE):
            program_fut = False


def billentyu_kezeles():
    global cica_x, cica_y
    for billenytu in lenyomott_bill:
        if lenyomott_bill[billenytu]:
            if billenytu == pygame.K_UP:
                if cica_y - cica_lepes >= 0 and mehet_fel:
                    cica_y -= cica_lepes
            elif billenytu == pygame.K_DOWN:
                if cica_y + cica_lepes <= 460 and mehet_le:
                    cica_y += cica_lepes
            elif billenytu == pygame.K_LEFT:
                if cica_x - cica_lepes >= 0 and mehet_balra:
                    cica_x -= cica_lepes
            elif billenytu == pygame.K_RIGHT:
                if cica_x + cica_lepes <= 450 and mehet_jobbra:
                    cica_x += cica_lepes


def labirintus():
    def labirintus_vonal(p1, p2):
        return pygame.Rect(p1[0], p1[1], max(abs(p2[0] - p1[0]), 1), max(abs(p2[1] - p1[1]), 1))

    return [
        labirintus_vonal((50, 50), (50, 200)),  # függőleges
        labirintus_vonal((100, 0), (100, 150)),  # függőleges
        labirintus_vonal((50, 200), (150, 200)),  # vízszintes
        labirintus_vonal((100, 100), (350, 100)),  # vízszintes
        labirintus_vonal((150, 50), (450, 50)),  # vízszintes
        labirintus_vonal((150, 150), (300, 150)),  # vízszintes
        labirintus_vonal((450, 50), (450, 100)),  # függőleges
        labirintus_vonal((400, 50), (400, 150)),  # függőleges
        labirintus_vonal((400, 150), (500, 150)),  # vízszintes
        labirintus_vonal((350, 100), (350, 200)),  # függőleges
        labirintus_vonal((0, 250), (200, 250)),  # vízszintes
        labirintus_vonal((200, 200), (250, 200)),  # vízszintes
        labirintus_vonal((350, 100), (350, 200)),  # függőleges
        labirintus_vonal((200, 200), (200, 250)),  # függőleges
        labirintus_vonal((250, 200), (250, 250)),  # függőleges
        labirintus_vonal((150, 150), (150, 200)),  # függőleges
        labirintus_vonal((300, 150), (300, 250)),  # függőleges
        labirintus_vonal((350, 200), (450, 200)),  # vízszintes
        labirintus_vonal((300, 250), (400, 250)),  # vízszintes
        labirintus_vonal((50, 300), (50, 400)),  # függőleges
        labirintus_vonal((100, 250), (100, 450)),  # függőleges
        labirintus_vonal((50, 450), (100, 450)),  # vízszintes
        labirintus_vonal((400, 250), (400, 300)),  # függőleges
        labirintus_vonal((450, 250), (450, 400)),  # függőleges
        labirintus_vonal((0, 400), (50, 400)),  # vízszintes
        labirintus_vonal((150, 300), (400, 300)),  # vízszintes
        labirintus_vonal((150, 300), (150, 500)),  # függőleges
        labirintus_vonal((200, 350), (200, 450)),  # függőleges
        labirintus_vonal((200, 350), (450, 350)),  # vízszintes
        labirintus_vonal((250, 400), (350, 400)),  # vízszintes
        labirintus_vonal((200, 450), (300, 450)),  # vízszintes
        labirintus_vonal((400, 450), (500, 450)),  # vízszintes
        labirintus_vonal((350, 400), (350, 500)),  # függőleges
        labirintus_vonal((400, 350), (400, 450))  # függőleges
    ]


def uj_cseresznye(x, y):
    print("uj cseresznye: ", x, y)
    return pygame.Rect(x, y, 5, 5)


def rajzolj_labirintust(labirintus):
    for v in labirintus:
        pygame.draw.rect(kepernyo, black, v)


labi = labirintus()

while program_fut:
    # minden ciklus elejen bealitjuk az alapertelmezett mozgasi ertekeket, amiket majd a 'labirintus_vonal' funkcioban modositunk
    mehet_jobbra = True
    mehet_balra = True
    mehet_fel = True
    mehet_le = True

    esemeny_kezeles()

    kepernyo.fill(white)  # RGB ertek, feher

    eger = uj_eger()
    eger_kerulet = eger.get_rect()
    eger_kerulet.center = (475, 475)
    kepernyo.blit(eger, eger_kerulet)

    cicc = cica(0, 0, labirintus=labi)

    rajzolj_labirintust(labi)

    if cseresznye is None:
        cseresznye = uj_cseresznye(random.randint(0, 500), random.randint(0, 500))

    if cicc.colliderect(cseresznye):
        print("cseresznye torolve: ", cseresznye.x, cseresznye.y)
        cseresznye = None
    else:
        pygame.draw.rect(kepernyo, red, cseresznye)

    billentyu_kezeles()

    pygame.display.flip()  # megjelenitjuk/rendereljuk, amit eddig "csak kirajzoltunk"

    ora.tick(100)

# Done! Time to quit.
pygame.quit()

# Külön objektumok: egér, macska, cseresznye, labirintus, játék