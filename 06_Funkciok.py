# PyCharm billentyukombinaciok
# CTRL+/ kikommentezes
# CTRL+ALT L : gyorsformazas

# szakszavak
# funkcio, fuggveny - function (ezt az elnevezest hasznaljuk legtobbszor)
# eljaras - procedure
# parameter, argumentum: valtozo, amely ertekei a funkcion kivulrol jonnek

# eljaras definicioja
def eljaras_neve():
    print("Ez a funkcio lefutott")
    print("Tobb sort is kiir")
    # return None # rejtett return (lasd "eljarasnak van eredmenye?" szekcio)


# eljaras meghivasa
eljaras_neve()  # elso hivas
eljaras_neve()  # masodik hivas


def fuggveny():
    print("fuggveny fut")
    return "eredmeny"


print("=========== fuggveny eredmenye es annak tipusa")
e = fuggveny()
print(e, type(e))

print("=========== fuggveny meghivas nelkul")
f = fuggveny  # hianyzik a (), nem hivodik meg
print(f, type(f))
print(f())  # meghivhato igy is


# ujabb fuggveny, parameterekkel
def sum(a, b):  # a es b parameterek, az ertekek kivulrol jonnek, ahol a fuggvenyt meghivjuk
    c = a + b
    return c


def sum_none_safe(a, b):  # a es b parameterek
    if a == None:
        return b
    if b == None:
        return a
    return a + b


# =======

e1 = 1
print("eredmeny:", sum(e1, 2))

# =======

print("====== eljarasnak van eredmenye?")
eljaras_eredmenye = eljaras_neve()
print(eljaras_eredmenye, type(eljaras_eredmenye))

# ====== nehany hibas fuggvenyhivas
# print(sum(None, 1)) # TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'
print(sum_none_safe(None, 1))
# print(sum(3, "hello")) # TypeError: unsupported operand type(s) for +: 'int' and 'str'
# sum()  # TypeError: sum() missing 2 required positional arguments: 'a' and 'b'


# ======

# mar ismert funkciok:
# print, type, input, int, str, float, bool, random.randint, random.random

# =======

print("========= korabbi HF megoldasa es tesztelese fuggvenyekkel")


def oszthato(szam):
    if szam % 3 == 0 and szam % 4 == 0:
        return "A megadott szám 3-mal és 4-gyel is osztható."
    elif szam % 3 == 0:
        return "A megadott szám csak 3-mal osztható."
    elif szam % 4 == 0:
        return "A megadott szám csak 4-gyel osztható."
    else:
        return "A megadott szám sem 3-mal, sem 4-gyel nem osztható"


def teszt(i):
    print(i, oszthato(i))


teszt(0)
teszt(1)
teszt(2)
teszt(3)
teszt(4)
teszt(5)
teszt(6)
teszt(7)
teszt(8)

# regebbi feladat atalakitva
num = int(input("Adj meg egy számot: "))


def milyen_szam(number):
    if number % 2 == 0 and number > 0:
        return "Pozitív páros szám"
    elif number % 2 == 0 and number < 0:
        return "Negatív páros szám"
    elif number % 2 == 1 and number > 0:
        return "Pozitív páratlan szám"
    elif number == 0:
        return "Nulla"
    else:
        return "Negatív páratlan szám"


print(milyen_szam(num))
